# nft-tutorial
A very basic NFT tutorial repository for absolute beginners in the world of Web3 and smart contracts

The completed version of the code developed by following our tutorial for NFT beginners [here](https://docs.opensea.io/docs/creating-an-nft-contract)

You'll need to update the environment variables `ALCHEMY_KEY` and `ACCOUNT_PRIVATE_KEY` in order to compile hardhat. You can learn more about where to get them [here](https://docs.alchemy.com/alchemy/tutorials/sending-txs).

[![Run on Repl.it](https://repl.it/badge/github/plibither8/2048.cpp)](https://replit.com/@openseaofficial/nft-tutorial)


1. Deploying contracts with the account: 0x03C1c537B4574081e64B44fAD2DF309276aD535C
Account balance: 100000000000000000
Contract deployed to address: 0x79a6b0B54743089b5781510197d8ED23E43ec4E2

2. deploy addess 0xE28DfD886A6e7e9106f1Ab2e802925C49e88bb78

command => npx hardhat mint --address 0x03C1c537B4574081e64B44fAD2DF309276aD535C

response => Transaction Hash: 0x036fafee85d39b9a668ef3a945a9024bd4ac6d3bd0637ba4791693af79a2cf46

3   npx ipfs-car --pack images --output images.car
root CID: bafybeibr6f2wccvyzgou6r2kklialooaexeste27aoaiqnhvmwu3h7b63e
image 1:bafkreihdjo4vehlf2v6k3iiwlhh5ib2vs5goknjk4bcqy2eg7q6fis5msa
    npx ipfs-car --pack metadata --output metadata.car

response => root CID: bafybeicav6q26rvxyfzclc7zlwinenlxuv6j6p3gy6abe6gpsao56ajici

npx hardhat set-base-token-uri --base-url "https://bafybeicav6q26rvxyfzclc7zlwinenlxuv6j6p3gy6abe6gpsao56ajici.ipfs.dweb.link/metadata/"






image car
root CID: bafybeidsl5rzhui2vymlgjqcdh22c3x2n7ebxuktcy7wrnygva7xowgypu

metadata car
root CID: bafybeihpvex6j3uvrwbh2mqmxunvalmxbmmdfpm4u2hxctkwkg7wnmreom

0x68acd837Ad2d4ea90fbDF82D90C87fb2Ffcbfe68

npx hardhat set-base-token-uri --base-url "https://bafybeihpvex6j3uvrwbh2mqmxunvalmxbmmdfpm4u2hxctkwkg7wnmreom.ipfs.dweb.link/metadata/"